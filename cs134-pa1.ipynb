{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Train a logistic regression model for text classification from scratch\n",
    "\n",
    "**Due September 27, 2019**\n",
    "\n",
    "## Overview\n",
    "\n",
    "Logistic regression (aka Maximum Entropy) has been one of the workhorses in natural language processing. It has also been used very widely in other fields. The model has many strengths. It is effective when you have a large number of features. It also handles the correlation between features really well. In addition, the time and space complexity for the model is not too taxing. All of these reasons make the Logistic Regression model a very versatile classifier for many NLP tasks. In this assignment, we are going to use the Logistic Regression model for sentiment analysis.\n",
    "\n",
    "\n",
    "## Getting ready\n",
    "\n",
    "\n",
    "Download the starter code along with the datasets using the link on latte. We highly recommend reviewing the slides before starting implementing.\n",
    "\n",
    "## Datasets\n",
    "\n",
    "- **Sentiment analysis on Yelp dataset.** We processed the data from Yelp dataset challenge and put it in the json format. Review class provided in corpus.py reads in the input for you. Each restaurant review is tagged with ‘negative’ (1-2 stars), ‘neutral’ (3 stars), or ‘positive’ (4-5 stars). We are building a classifier that labels a review as one of the three types of sentiments based on the review text. \n",
    "\n",
    "- **Name classification.** This should be used to test and debug your code. You can just use the first character and the last character as your features.\n",
    "\n",
    "## What needs to be done\n",
    "\n",
    "A Logistic Regression model has a lot of moving parts. The list below guides you through what needs to be done. The three things you need to focus on are: representation, learning, and inference. \n",
    "\n",
    "### Representation \n",
    "- Choose the feature set. Start with the feature set that is small or the learning algorithm will take a long time to run and this makes the debugging process difficult. You can ramp up the number and variety of features when your code is thoroughly tested. \n",
    "- Choose the data structure that holds the features. We recommend sparse feature vectors. Regardless of your choice, cache the features internally within each Document object as the algorithm is iterative. Featurization should be done only once.\n",
    "- Choose the data structures that hold the parameters. We recommend using a $k \\times p$ matrix where $k$ is the number of labels, and $p$ is the number of linguistic features. \n",
    "\n",
    "### Learning\n",
    "- Compute the negative log-likelihood function given a minibatch of data. You will need this function to track progress of parameter fitting. \n",
    "- Compute the gradient with respect to the parameters. You will need the gradient for updating the parameters given a minibatch of data. \n",
    "- Implement the mini-batch gradient descent algorithm to train the model to obtain the best parameters.\n",
    "\n",
    "\n",
    "### Classification/Inference.\n",
    "- Apply the model to unseen data. The provided test cases will evaluate the model on both datasets, and you must pass those.\n",
    "\n",
    "### Experiments\n",
    "\n",
    "In addition to implementing the mini-batch gradient descent algorithm for the Logistic Regression model, you are asked to do the following experiments to better understand the behavior of the model. For all three experiments, use the Yelp review dataset as it is a more realistic one. \n",
    "\n",
    "\n",
    "**Experiment 1 -- Training set size**: \n",
    "\n",
    "Does the size of the training set really matter? The mantra of machine learning tells us that the bigger the training set the better the performance. We will investigate how true this is. \n",
    "\n",
    "In this experiment, fix the feature set to something reasonable and fix the dev set and the test set. Vary the size of the training set $\\{1000, 10000, 50000, 100000$, and all} and compare the (peak) accuracy from each training set size. Make a plot of size vs accuracy. Analyze and write up a short paragraph on what you learn or observe from this experiment. \n",
    "\n",
    "**Experiment 2--- Minibatch size**: \n",
    "\n",
    "Why are we allowed to use mini-batch instead of the whole training set when updating the parameters? This is indeed the dark art of this optimization algorithm, which works well for many complicated models, including neural networks. Computing gradient is always expensive, so we want to know how much we gain from each gradient computation.\n",
    "\n",
    "In this experiment, try minibatch sizes $\\{1, 10, 50, 100, 1000\\}$, using the best training size from Experiment 1. For each mini-batch size, plot the number of datapoints that you compute the gradient for  (x-axis) against the accuracy of the development set (y-axis). Analyze and write up a short paragraph on what you learn or observe from this experiment. \n",
    "\n",
    "**Experiment 3 -- Hyperparameter tuning**: \n",
    "\n",
    "Try different values of $\\lambda = \\{0.1, 0.5, 1, 10\\}$ for $L2$ reguarlization and observe its effect on the accuracy of the model against the development set. Make a plot of  lambda value vs accuracy on the development set. Write a short paragraph summarizing what you have oberved from this experiment.\n",
    "\n",
    "As you are doing more experiments, the number of experimental settings starts to multiply. Use your best settings from your Experiment 1 and your Experiment 2 for the tuning of the $L2$ regularization parameters. It's not advisable to vary more than one experimental variable at a time, and that'll make it hard to interpret your results. You can set up a grid search procedure to do this experiment automatically without manual intervention.\n",
    "\n",
    "**Experiment 4 -- Feature Engineering (Extra credit for creative and effective features)**: In addition to bag-of-words features, experiment with additional features (bigrams, trigrams, etc.) to push up the performance of the model as much as you can. The addition of new features should be driven by error analysis. This process is similar to the machine learning process itself, only that it involves actual humans looking at the the errors made of the machine learning model and trying to come up with new features to fix or reduce those errors. Briefly describe what new features you have tried if they are useful.\n",
    "\n",
    "\n",
    "## Submission\n",
    "\n",
    "Submit the following on Latte:\n",
    "\n",
    "### All your code.\n",
    "But don’t include the datasets as we already have those.\n",
    "\n",
    "### Report.\n",
    "Please include the following sections in your report:\n",
    "1. A brief explanation of your code structure\n",
    "\n",
    "2. How to run your code, and what output to expect\n",
    "\n",
    "3. Experimental settings \n",
    "(Explain clearly what feature set is being used and how you set up mini-batch gradient descent because there can be quite a bit of variations.)\n",
    "\n",
    "4. Experimental results\n",
    "\n",
    "Please keep this report no more than two pages single-spaced including graphs. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## More on Mini-batch Gradient Descent\n",
    "\n",
    "In this assignment, we will train Logistic Regression models using mini-batch gradient descent. Gradient descent learns the parameter by iterative updates given a chunk of data and its gradient. \n",
    "\n",
    "If a chunk of data is the entire training set, we call it batch gradient descent. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'converged' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-1-784dedabe20f>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[1;32m----> 1\u001b[1;33m \u001b[1;32mwhile\u001b[0m \u001b[1;32mnot\u001b[0m \u001b[0mconverged\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m      2\u001b[0m     \u001b[0mgradient\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mcompute_gradient\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mparameters\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mtraining_set\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m      3\u001b[0m     \u001b[0mparameters\u001b[0m \u001b[1;33m-=\u001b[0m \u001b[0mgradient\u001b[0m \u001b[1;33m*\u001b[0m \u001b[0mlearning_rate\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;31mNameError\u001b[0m: name 'converged' is not defined"
     ]
    }
   ],
   "source": [
    "while not converged:\n",
    "    gradient = compute_gradient(parameters, training_set)\n",
    "    parameters -= gradient * learning_rate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Batch gradient descent is much slower. The gradient from the entire dataset needs to be computed for each update. This is usually not necessary. Computing gradient from a smaller subset of the data at a time usually gives the same results if done repeatedly. \n",
    "\n",
    "If a subset of the training set is used to compute the gradient, we call it mini-batch gradient descent. This approximates the gradient of batch gradient descent. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "while not converged:\n",
    "    minibatches = chop_up(training_set)\n",
    "    for minibatch in minibatches:\n",
    "        gradient = compute_gradient(parameters, minibatch)\n",
    "        parameters -= gradient * learning_rate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If a chunk of data is just one instance from the training set, we call it stochastic gradient descent (SGD). Each update only requires the computation of the gradient of one data instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "while not converged:\n",
    "    for datapoint in training_set:\n",
    "        gradient = compute_gradient(parameters, datapoint)\n",
    "        parameters -= gradient * learning_rate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Practical issues with mini-batch gradient descent\n",
    "\n",
    "- How should I initialize the parameters at the first iteration?\n",
    "\n",
    "    Set them all to zero. This is generally not advisable for more complicated models. But for the Logistic Regression model, zero initialization works perfectly. \n",
    "\n",
    "\n",
    "- How do I introduce the bias term?\n",
    "\n",
    "    Include a feature that fires in ALL data instances. And treat it as a normal feature and proceed as usual. \n",
    "\n",
    "\n",
    "- Why do the posterior P(Y|X) become NaN? \n",
    "\n",
    "    It is very likely that you exponentiate some big number and divide by the same amount i.e. if unnoramlized_score is a vector of unnormalized scores (the sum of lambdas), then:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "             posterior = exp(unnormalized_score) / sum(exp(unnormalized_score)) \n",
    "     \n",
    "   > This is no good. We have to get around by using some math tricks:\n",
    "   \n",
    "             posterior = exp(unnormalized_score - scipy.misc.logsumexp(unnormalized_score))\n",
    "     \n",
    "   > If this confuses you or you are not sure why this is correct, think about it more or ask the TAs. But we are quite confident that you will need to use the logsumexp function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "- How do you know that it converges? \n",
    "\n",
    "    It is extremely difficult to know. If you stop too early, the model has not reached its peak yet i.e. *underfitting*. If you stop too late, the model will fit too well to the training set and not generalize to the unseen data i.e. *overfitting*. But there are multiple ways to guess the convergence. We suggest this method called *early stopping*.\n",
    "\n",
    "    Every once in a while evaluate the model on the development set during gradient descent.\n",
    "\n",
    "    - If the performance is better than last evaluation, then save this set of parameters and keep going for a little more. \n",
    "    - If the performance stops going up after a few updates, stop and use the last saved parameters. (How many is a few? Up to you)\n",
    "\n",
    "\n",
    "- How often should I run evaluation on the dev set during training? \n",
    "\n",
    "    Up to you. It is actually OK to run the evaluation on the dev at every update you make to the parameters.\n",
    "\n",
    "\n",
    "- How do I know that my implementation is correct?\n",
    "\n",
    "    Look at the average negative log-likelihood. It should keep going down monotonically i.e. at every single update. You should also see that the gradient should get closer and closer to zero."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
