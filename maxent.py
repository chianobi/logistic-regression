# -*- mode: Python; coding: utf-8 -*-

from classifier import Classifier
import numpy as np
from scipy.special import logsumexp


class MaxEnt(Classifier):

    def __init__(self):
        self.parameters = None
        self.ling_features = {}
        self.labels = {}

    def get_model(self): return None

    def set_model(self, model): pass

    model = property(get_model, set_model)

    def train(self, instances, dev_instances=None):
        """Construct a statistical model from labeled instances."""
        label_num = 0
        self.labels = {}
        feature_num = 0
        for inst in instances:
            features = list(inst.features())
            for feat in features:
                if feat not in self.ling_features:
                    self.ling_features[feat] = feature_num
                    feature_num += 1
            if inst.label not in self.labels.values():
                self.labels[label_num] = inst.label
                label_num += 1
        self.ling_features['bias'] = feature_num
        self.parameters = np.zeros([len(self.labels), len(self.ling_features)])
        for inst in instances:
            self.featurize(inst)

        self.train_sgd(instances, dev_instances, 0.0001, 50)

    def train_sgd(self, train_instances, dev_instances, learning_rate, batch_size):
        """Train MaxEnt model with Mini-batch Stochastic Gradient"""
        epoch = 0
        converged = 0
        dips = 0
        saved_params = self.parameters
        accs = []
        while not converged:
            minibatches = list(chop_up(train_instances, batch_size))
            for minibatch in minibatches:
                gradient = compute_gradient(self.parameters, minibatch, self.labels, self.ling_features, 0.5)
                self.parameters -= gradient * learning_rate

            # test on dev set, see if we can stop
            correct = [self.classify(x) == x.label for x in dev_instances]
            dev_acc = sum(correct) / len(correct)
            accs.append(dev_acc)

            if epoch == 0:
                max_acc = accs[0]
            else:
                if dev_acc > max_acc:
                    max_acc = dev_acc
                    saved_params = self.parameters
                    dips = 0
                else:
                    if dev_acc < accs[epoch-1]:
                        dips += 1
            if dips > 3 and epoch > 50:
                converged += 1
                self.parameters = saved_params
            epoch += 1

    def classify(self, instance):
        if instance.feature_vector == []:
            self.featurize(instance)
        fv = instance.feature_vector

        scores = prediction(self.parameters, fv)
        posterior = np.exp(scores - logsumexp(scores))
        return self.labels[np.argmax(posterior)]

    def featurize(self, instance):
        vector = np.zeros(len(self.ling_features))
        for feat in list(instance.features()):
            if feat in self.ling_features:
                index = self.ling_features[feat]
                vector[index] += 1
        vector[self.ling_features['bias']] = 1
        instance.feature_vector = vector


def compute_gradient(parameters, minibatch, labels, features, lamda):
    gradient = np.zeros([len(labels), len(features)])
    for inst in minibatch:
        fv = inst.feature_vector
        y = inst.label
        scores = prediction(parameters, fv)
        posterior = np.exp(scores - logsumexp(scores))
        expected = np.zeros([len(labels), len(fv)])
        for i, val in labels.items():
            if y == val:
                index = i
            expected_i = np.zeros([len(labels), len(fv)])
            expected_i[i] = fv
            expected_i *= posterior[i]
            expected += expected_i

        observed = np.zeros([len(labels), len(fv)])
        observed[index] = fv
        observed = np.negative(observed)
        reg = lamda * parameters
        gradient += (observed + expected)
    return (gradient + reg)


def prediction(theta, X):
    return np.dot(theta, X)


def chop_up(dataset, batch_size):
    for i in range(0, len(dataset), batch_size):
        yield dataset[i:i+batch_size]
